
import numpy as np
_A = np.array  # A shortcut to creating arrays in command line 
import os
import cv2
import sys
import csv
from pickle import load, dump
from DataRow import DataRow, BBox, ErrorAcum, Predictor, getGitRepFolder

###########################    PATHS TO SET   ####################
# Set CAFFE_ROOT in your bash, or set it here if not found
CAFFE_ROOT = os.environ.get('CAFFE_ROOT','/Users/ishay/caffe/distribute')
sys.path.append(CAFFE_ROOT+'/python')  # Add caffe python path so we can import it
import caffe

DLIB_ROOT=os.environ.get('DLIB_ROOT','/Applications/anaconda/lib/python2.7/site-packages/dlib-18.18.99-py2.7-macosx-10.5-x86_64.egg')
sys.path.append(DLIB_ROOT)
import dlib


ROOT = getGitRepFolder()  # ROOT is the git root folder .
sys.path.append(os.path.join(ROOT, 'python'))  # Assume git root directory
DATA_PATH = os.path.join(ROOT, 'data', 'train')
CSV_TEST  = os.path.join(ROOT, 'data', 'train', 'testImageList.txt')
CSV_TRAIN = os.path.join(ROOT, 'data', 'train', 'trainImageList.txt')

MEAN_TRAIN_SET = cv2.imread(os.path.join(ROOT, 'trainMean.png')).astype('f4')
STD_TRAIN_SET  = cv2.imread(os.path.join(ROOT, 'trainSTD.png')).astype('f4')

###########################    STEPS TO RUN       ####################
FULL_STEPS=['testSetHD5', 'testSetPickle', 'trainSetHD5', 'testError', 'createAFLW_TestSet', 'createAFW_TestSet', 'testAFLW_TestSet', 'testAFW_TestSet', 'testErrorMini']  # All the steps needed.
STEPS =FULL_STEPS  # ['testAFW_TestSet'] #override this to run partial steps ['testErrorMini']


detector=dlib.get_frontal_face_detector()


class RetVal:
    pass  ## A generic class to return multiple values without a need for a dictionary.

def createDataRowsFromCSV(csvFilePath, csvParseFunc, DATA_PATH, limit = sys.maxint):
    ''' Returns a list of DataRow from CSV files parsed by csvParseFunc, 
        DATA_PATH is the prefix to add to the csv file names,
        limit can be used to parse only partial file rows.
    ''' 
    data = []  # the array we build
    validObjectsCounter = 0 
    
    with open(csvFilePath, 'r') as csvfile:

        reader = csv.reader(csvfile, delimiter=' ')
        for row in reader:
            d = csvParseFunc(row, DATA_PATH)
            if d is not None:
                data.append(d)
                validObjectsCounter += 1
                if (validObjectsCounter > limit ):  # Stop if reached to limit
                    return data 
    return data

def getValidWithBBox(dataRows):
    ''' Returns a list of valid DataRow of a given list of dataRows 
    '''
    R=RetVal()
    
    R.outsideLandmarks = 0 
    R.noImages = 0 
    R.noFacesAtAll = 0 
    R.couldNotMatch = 0
    
    validRow=[]
    for dataRow in dataRows:
        if dataRow.image is None or len(dataRow.image)==0:
            R.noImages += 1
        lmd_xy = dataRow.landmarks().reshape([-1,2])
        left,  top = lmd_xy.min( axis=0 )
        right, bot = lmd_xy.max( axis=0 )
                
        dets = detector( np.array(dataRow.image, dtype = 'uint8' ) );
        
        det_bbox = None  # the valid bbox if found 
    
        for det in dets:
            det_box = BBox.BBoxFromLTRB(det.left(), det.top(), det.right(), det.bottom())
            
            # Does all landmarks fit into this box?
            if top >= det_box.top and bot<= det_box.bottom and left>=det_box.left and right<=det_box.right:
                det_bbox = det_box  
                    
        if det_bbox is None:
            if len(dets)>0:
                R.couldNotMatch += 1  # For statistics, dlib found faces but they did not match our landmarks.
            else:
                R.noFacesAtAll += 1  # dlib found 0 faces.
        else:
            dataRow.fbbox = det_bbox  # Save the bbox to the data row
            if det_bbox.left<0 or det_bbox.top<0 or det_bbox.right>dataRow.image.shape[0] or det_bbox.bottom>dataRow.image.shape[1]:
                R.outsideLandmarks += 1  # Saftey check, make sure nothing goes out of bound.
            else:
                validRow.append(dataRow)  
    
    
    return validRow,R 
        
def writeHD5(dataRows, outputPath, setTxtFilePATH, meanTrainSet, stdTrainSet , IMAGE_SIZE=40):
    ''' Create HD5 data set for caffe from given valid data rows. 
    '''
    from numpy import zeros
    import h5py
    
    BATCH_SIZE = len(dataRows)
    HD5Images = zeros([BATCH_SIZE, 3, IMAGE_SIZE, IMAGE_SIZE], dtype='float32')
    HD5Landmarks = zeros([BATCH_SIZE, 10], dtype='float32')
    #prefix  = os.path.join(ROOT, 'caffeData', 'hd5', 'train')
    setTxtFile = open(setTxtFilePATH, 'w')

    
    for i, dataRowOrig in enumerate(dataRows):
        if i % 1000 == 0 or i >= len(dataRows)-1:
            print "Processing row %d " % i 
            
        if not hasattr(dataRowOrig, 'fbbox'):
            print "Warning, no fbbox"
            continue
        
        dataRow = dataRowOrig.copyCroppedByBBox(dataRowOrig.fbbox)  # Get a cropped scale copy of the data row
        scaledLM = dataRow.landmarksScaledMinus05_plus05() 
        image = dataRow.image.astype('f4')
        image = (image-meanTrainSet)/(1.e-6 + stdTrainSet)
        
        HD5Images[i, :] = cv2.split(image)  # split interleaved (40,40,3) to (3,40,40)
        HD5Landmarks[i,:] = scaledLM
        
    with h5py.File(outputPath, 'w') as T:
        T.create_dataset("X", data=HD5Images)
        T.create_dataset("landmarks", data=HD5Landmarks)

    setTxtFile.write(outputPath+"\n")
    setTxtFile.flush()
    setTxtFile.close()
    
    
    
  
##########################################    SCRIPT STEPS       ##################################################

if 'testSetHD5' in STEPS or 'testSetPickle' in STEPS:
    dataRowsTest_CSV  = createDataRowsFromCSV(CSV_TEST , DataRow.DataRowFromNameBoxInterlaved, DATA_PATH)
    print "Finished reading %d rows from test" % len(dataRowsTest_CSV)
    dataRowsTestValid,R = getValidWithBBox(dataRowsTest_CSV)
    print "Original test:",len(dataRowsTest_CSV), "Valid Rows:", len(dataRowsTestValid), " noFacesAtAll", R.noFacesAtAll, " outside:", R.outsideLandmarks, " couldNotMatch:", R.couldNotMatch
    if 'testSetPickle' in STEPS:
        with open('testSet.pickle','w') as f:
            dump(dataRowsTestValid,f)
        print "Finished dumping to testSet.pickle"
        # Also save mini test set for debug
        with open('testSetMini.pickle','w') as f:
            dump(dataRowsTestValid[0:10],f)
        print "Finished dumping to testSetMini.pickle"
        
        
    if 'testSetHD5' in STEPS:
        writeHD5(dataRowsTestValid, ROOT+'/caffeData/hd5/test.hd5', ROOT+'/caffeData/test.txt', MEAN_TRAIN_SET, STD_TRAIN_SET)
        print "Finished writing test to caffeData/test.txt"

if 'trainSetHD5' in STEPS:
    dataRowsTrain_CSV = createDataRowsFromCSV(CSV_TRAIN, DataRow.DataRowFromNameBoxInterlaved, DATA_PATH)
    print "Finished reading %d rows from train" % len(dataRowsTrain_CSV)
    dataRowsTrainValid,R = getValidWithBBox(dataRowsTrain_CSV)
    print "Original train:",len(dataRowsTrain_CSV), "Valid Rows:", len(dataRowsTrainValid), " noFacesAtAll", R.noFacesAtAll, " outside:", R.outsideLandmarks, " couldNotMatch:", R.couldNotMatch
    writeHD5(dataRowsTrainValid, ROOT+'/caffeData/hd5/train.hd5', ROOT+'/caffeData/train.txt', MEAN_TRAIN_SET, STD_TRAIN_SET)
    print "Finished writing train to caffeData/train.txt"
    

DEBUG = False
###
if 'testError' in STEPS:
    with open('testSet.pickle','r') as f:
        dataRowsTrainValid = load(f)
        
    testError=ErrorAcum()
    predictor = Predictor(protoTXTPath = ROOT+'/caffeData/vanilaCNN.prototxt', weightsPath= ROOT+'/caffeData/vanilaCNN.caffemodel')
    for i, dataRow in enumerate(dataRowsTrainValid):
        dataRow40 = dataRow.copyCroppedByBBox(dataRow.fbbox)
        image, lm40 = predictor.preprocess(dataRow40.image, dataRow40.landmarks())
        prediction = predictor.predict(image)
        testError.add(lm40, prediction)
        dataRow40.prediction = (prediction+0.5)*40.
        
        if DEBUG:
            dataRow.prediction = dataRow40.inverseScaleAndOffset(dataRow40.prediction)
            dataRow.show()
            break
        
            
    print "Test Error:", testError


DEBUG = True
if 'testErrorMini' in STEPS:
    with open('testSetMini.pickle','r') as f:
        dataRowsTrainValid = load(f)
        
    testErrorMini=ErrorAcum()
    predictor = Predictor(protoTXTPath = ROOT+'/caffeData/vanilaCNN.prototxt', weightsPath= ROOT+'/caffeData/vanilaCNN.caffemodel')
    for i, dataRow in enumerate(dataRowsTrainValid):
        dataRow40 = dataRow.copyCroppedByBBox(dataRow.fbbox)
        image, lm_0_5 = predictor.preprocess(dataRow40.image, dataRow40.landmarks())
        prediction = predictor.predict(image)
        testErrorMini.add(lm_0_5, prediction)
        dataRow40.prediction = (prediction+0.5)*40.  # Scale -0.5..+0.5 to 0..40
        
        if DEBUG:
            dataRow.prediction = dataRow40.inverseScaleAndOffset(dataRow40.prediction) # Scale up to the original image scale
            dataRow.show()
            if i>40:
                print "Debug breaked after %d rows:" % i
            

    print "Test Error mini:", testErrorMini

    
    
#%% AFW test - Make the pickle data set
if 'createAFW_TestSet' in STEPS:
    from scipy.io import loadmat
    AFW_DATA_PATH = os.path.join(ROOT, 'data', 'AFW')
    AFW_MAT_PATH = os.path.join(AFW_DATA_PATH, 'anno-v7.mat')
    annotaions = loadmat(AFW_MAT_PATH)['anno']
    dataRowsAFW = []
        
    for anno in annotaions:
        dataRow = DataRow.DataRowFromAFW(anno, AFW_DATA_PATH)
        if dataRow is not None:
            dataRowsAFW.append(dataRow)
    print "Finished parsing anno-v7.mat with total rows:", len(dataRowsAFW)
    annotaions = None  # remove from memory
    
    dataRowsAFW_Valid, R=getValidWithBBox(dataRowsAFW)
    print "Original AFW:",len(dataRowsAFW), "Valid Rows:", len(dataRowsAFW_Valid), " noFacesAtAll", R.noFacesAtAll, " outside:", R.outsideLandmarks, " couldNotMatch:", R.couldNotMatch
    dataRowsAFW = None  # remove from Memory
    with open('afwTestSet.pickle','w') as f:
        dump(dataRowsAFW_Valid, f)
        print "Data saved to afwTestSet.pickle"
    
    
DEBUG = False
if 'testAFW_TestSet' in STEPS:
    with open('afwTestSet.pickle','r') as f:
        dataRowsAFW_Valid = load(f)

    testErrorAFW=ErrorAcum()
    predictor = Predictor(protoTXTPath = ROOT+'/caffeData/vanilaCNN.prototxt', weightsPath= ROOT+'/caffeData/vanilaCNN.caffemodel')
    for i, dataRow in enumerate(dataRowsAFW_Valid):
        dataRow40 = dataRow.copyCroppedByBBox(dataRow.fbbox)
        image, lm_0_5 = predictor.preprocess(dataRow40.image, dataRow40.landmarks())
        prediction = predictor.predict(image)
        testErrorAFW.add(lm_0_5, prediction)
        dataRow40.prediction = (prediction+0.5)*40.  # Scale -0.5..+0.5 to 0..40
        
        if DEBUG:
            dataRow.prediction = dataRow40.inverseScaleAndOffset(dataRow40.prediction) # Scale up to the original image scale
            dataRow.show()
            

    print "Test Error AFW:", testErrorAFW

    


if 'createAFLW_TestSet' in STEPS:  # Also known as MTFL
    AFLW_PATH = os.path.join(ROOT,'data','MTFL')
    CSV_MTFL = os.path.join(AFLW_PATH,'testing.txt')
    dataRowsMTFL_CSV  = createDataRowsFromCSV(CSV_MTFL , DataRow.DataRowFromMTFL, AFLW_PATH)
    print "Finished reading %d rows from test" % len(dataRowsMTFL_CSV)
    dataRowsMTFLValid,R = getValidWithBBox(dataRowsMTFL_CSV)
    print "Original test:",len(dataRowsMTFL_CSV), "Valid Rows:", len(dataRowsMTFLValid), " noFacesAtAll", R.noFacesAtAll, " outside:", R.outsideLandmarks, " couldNotMatch:", R.couldNotMatch
    with open('testSetMTFL.pickle','w') as f:
        dump(dataRowsMTFLValid,f)
    print "Finished dumping to testSetMTFL.pickle"        


DEBUG = False
if 'testAFLW_TestSet' in STEPS:
    with open('testSetMTFL.pickle','r') as f:
        dataRowsAFW_Valid = load(f)

    testErrorAFLW=ErrorAcum()
    predictor = Predictor(protoTXTPath = ROOT+'/caffeData/vanilaCNN.prototxt', weightsPath= ROOT+'/caffeData/vanilaCNN.caffemodel')
    for i, dataRow in enumerate(dataRowsAFW_Valid):
        dataRow40 = dataRow.copyCroppedByBBox(dataRow.fbbox)
        image, lm_0_5 = predictor.preprocess(dataRow40.image, dataRow40.landmarks())
        prediction = predictor.predict(image)
        testErrorAFLW.add(lm_0_5, prediction)
        dataRow40.prediction = (prediction+0.5)*40.  # Scale -0.5..+0.5 to 0..40
        
        if DEBUG:
            dataRow.prediction = dataRow40.inverseScaleAndOffset(dataRow40.prediction) # Scale up to the original image scale
            dataRow.show()
            if i > 40:
                print "Debug finished after %d rows." % i
                break

    print "Test Error AFLW:", testErrorAFLW


