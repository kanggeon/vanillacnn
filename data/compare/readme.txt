This package contains ground truth and results of us and those compared in our paper [4]. Our results are generated
directly from the executable files provided in our project website [1]. Results of Liang et al. [5], Valstar et al. [6], Luxand [2], and
Microsoft [3] are acquired by running their codes or binaries. For methods of Belhumeur et al. [7] and Cao et al. [8], we compared
with the reported results in their papers, and We do not have their results for each point in each image. For methods [5,6,2,3],
we compared for faces that can be detected by all the methods on both BioID and LFPW (both training and testing images) datasets.
We also tested our method on the 249 LFPW test images used in [8], in order to compare with [7,8]. Our ground truth positions on
BioID and LFPW are a little different from those provided by the datasets to be consistent with our training data.

A brief description of the files contained in this package.
 our results
  *_bbox.txt (Our face detection results)
  *_result.bin (Our facial point detection results. Please refer to the code package for its format.)
 Results of other methods
  facial_points_positions_*_CBDS.txt (Liang et al. [5])
  facial_points_positions_*_BoRMaN.txt (Valstar et al. [6])
  facial_points_positions_*_Luxand.txt (Luxand [2])
  facial_points_positions_*_winPhone.txt (Microsoft [3])
 ground truth
  facial_points_positions_*_label.txt
 matlab scripts for method comparison
  compare_bioid.m
  compare_lfpw.m
 matlab scripts for testing our method on the 249 LFPW test images
  convnet_lfpw_test.m
 results generated by the matlab scripts
  compare_bioid.txt
  compare_lfpw.txt
  convnet_lfpw_test.txt

References
 [1] http://mmlab.ie.cuhk.edu.hk/CNN_FacePoint.htm
 [2] http://www.luxand.com/facesdk/.
 [3] http://research.microsoft.com/en-us/projects/facesdk/.
 [4] Y. Sun, X. Wang, and X. Tang. Deep Convolutional Network Cascade for Facial Point Detection. In Proc. CVPR, 2013.
 [5] L. Liang, R. Xiao, F. Wen, and J. Sun. Face alignment via component-based discriminative search. In Proc. ECCV, 2008.
 [6] M. Valstar, B. Martinez, X. Binefa, and M. Pantic. Facial point detection using boosted regression and graph models. In Proc. CVPR, 2010.
 [7] P. N. Belhumeur, D. W. Jacobs, D. J. Kriegman, and N. Kumar. Localizing parts of faces using a consensus of exemplars. In Proc. CVPR, 2011.
 [8] X. Cao, Y. Wei, F. Wen, and J. Sun. Face alignment by explicit shape regression. In Proc. CVPR, 2012.
