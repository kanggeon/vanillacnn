clear all
close all
clc

fid1 = fopen('facial_points_positions_lfpw_test_249_label.txt', 'r');
imageNum = fscanf(fid1, '%d', 1);
ground.id = zeros(imageNum, 1);
ground.pt = zeros(imageNum, 10);
for n1 = 1 : imageNum
    str = fscanf(fid1, '%s', 1);
    pos = strfind(str, '\');
    ground.id(n1) = str2double(str(pos(end) + 1 : end - 4));
    ground.pt(n1, :) = fscanf(fid1, '%f', 10)';
end
fclose(fid1);

fid1 = fopen('lfpw_test_249_result.bin', 'rb');
imageNum = fread(fid1, 1, 'int32');
assert(imageNum == 249);
pointNum = fread(fid1, 1, 'int32');
assert(pointNum == 5);
valid = fread(fid1, imageNum, 'int8');
assert(all(valid) == 1);
convnet.pt = reshape(fread(fid1, 2 * pointNum * imageNum, 'float64'),...
    [2 * pointNum, imageNum])';
fclose(fid1);
fid1 = fopen('lfpw_test_249_bbox.txt', 'r');
convnet.id = zeros(imageNum, 1);
for n1 = 1 : imageNum
    str = fscanf(fid1, '%s', 1);
    fscanf(fid1, '%d', 4);
    pos = strfind(str, '\');
    convnet.id(n1) = str2double(str(pos(end) + 1 : end - 4));
end
fclose(fid1);

imageNum = 249;
err_convnet = zeros(imageNum, 5);
c1 = 0;
for n1 = 1 : imageNum
    id = ground.id(n1);
    id_convnet = convnet.id == id;
    if any(id_convnet)
        c1 = c1 + 1;
        gro = ground.pt(n1, :);
        dist = sqrt((gro(1) - gro(3)) .^ 2 + (gro(2) - gro(4)) .^ 2);
        poi = convnet.pt(id_convnet, :);
        err_convnet(c1, :) = sqrt(sum(reshape((gro - poi) .^ 2, 2, 5))) / dist;
    end
end

err_convnet = err_convnet(1 : c1, :);

avgErr_convnet = mean(err_convnet) .* 100;
fail_convnet = sum(err_convnet > 0.1) ./ c1 .* 100;

fid1 = fopen('convnet_lfpw_test.txt', 'w');
fprintf(fid1, 'image tested: %d\r\n\r\n', c1);

fprintf(fid1, 'average normalized error (%%)\r\n\r\n');

fprintf(fid1, 'convnet\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    avgErr_convnet(1), avgErr_convnet(2), avgErr_convnet(3), avgErr_convnet(4), avgErr_convnet(5));

fprintf(fid1, 'failure rate (%%)\r\n\r\n');

fprintf(fid1, 'convnet\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    fail_convnet(1), fail_convnet(2), fail_convnet(3), fail_convnet(4), fail_convnet(5));

fclose('all');
