image tested: 249

average normalized error (%)

convnet
LE 2.37
RE 2.15
N  2.32
LM 2.56
RM 2.49

failure rate (%)

convnet
LE 1.20
RE 0.40
N  0.40
LM 1.61
RM 1.20

