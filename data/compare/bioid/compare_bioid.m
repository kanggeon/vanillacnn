clear all
close all
clc

fid1 = fopen('facial_points_positions_bioid_1521_label.txt', 'r');
imageNum = fscanf(fid1, '%d', 1);
ground.id = zeros(imageNum, 1);
ground.pt = zeros(imageNum, 10);
for n1 = 1 : imageNum
    str = fscanf(fid1, '%s', 1);
    pos = strfind(str, '\');
    ground.id(n1) = str2double(str(pos(end) + 1 : end - 4));
    ground.pt(n1, :) = fscanf(fid1, '%f', 10)';
end
fclose(fid1);

fid1 = fopen('bioid_1471_result.bin', 'rb');
imageNum = fread(fid1, 1, 'int32');
assert(imageNum == 1471);
pointNum = fread(fid1, 1, 'int32');
assert(pointNum == 5);
valid = fread(fid1, imageNum, 'int8');
assert(all(valid) == 1);
convnet.pt = reshape(fread(fid1, 2 * pointNum * imageNum, 'float64'),...
    [2 * pointNum, imageNum])';
fclose(fid1);
fid1 = fopen('bioid_1471_bbox.txt', 'r');
convnet.id = zeros(imageNum, 1);
for n1 = 1 : imageNum
    str = fscanf(fid1, '%s', 1);
    fscanf(fid1, '%d', 4);
    pos = strfind(str, '\');
    convnet.id(n1) = str2double(str(pos(end) + 1 : end - 4));
end
fclose(fid1);

fid1 = fopen('facial_points_positions_bioid_1453_CBDS.txt', 'r');
cbds.id = zeros(1453, 1);
cbds.pt = zeros(1453, 10);
for n1 = 1 : 1453
    str = fscanf(fid1, '%s', 1);
    pos = strfind(str, '\');
    cbds.id(n1) = str2double(str(pos(end) + 1 : end - 4));
    cbds.pt(n1, :) = fscanf(fid1, '%f', 10)';
end
fclose(fid1);

fid1 = fopen('facial_points_positions_bioid_1416_BoRMaN.txt', 'r');
borman.id = zeros(1416, 1);
borman.pt = zeros(1416, 10);
for n1 = 1 : 1416
    str = fscanf(fid1, '%s', 1);
    pos = strfind(str, '\');
    borman.id(n1) = str2double(str(pos(end) + 1 : end - 4));
    borman.pt(n1, :) = fscanf(fid1, '%f', 10)';
end
fclose(fid1);

fid1 = fopen('facial_points_positions_bioid_1503_Luxand.txt', 'r');
luxand.id = zeros(1503, 1);
luxand.pt = zeros(1503, 10);
for n1 = 1 : 1503
    str = fscanf(fid1, '%s', 1);
    pos = strfind(str, '\');
    luxand.id(n1) = str2double(str(pos(end) + 1 : end - 4));
    luxand.pt(n1, :) = fscanf(fid1, '%f', 10)';
end
fclose(fid1);

fid1 = fopen('facial_points_positions_bioid_1471_winPhone.txt', 'r');
winPhone.id = zeros(1471, 1);
winPhone.pt = zeros(1471, 4);
for n1 = 1 : 1471
    str = fscanf(fid1, '%s', 1);
    pos = strfind(str, '\');
    winPhone.id(n1) = str2double(str(pos(end) + 1 : end - 4));
    winPhone.pt(n1, :) = fscanf(fid1, '%f', 4)';
end
fclose(fid1);

imageNum = 1521;
err_convnet = zeros(imageNum, 5);
err_cbds = zeros(imageNum, 5);
err_borman = zeros(imageNum, 5);
err_luxand = zeros(imageNum, 5);
err_winPhone = zeros(imageNum, 2);
c1 = 0;
for n1 = 1 : imageNum
    id = ground.id(n1);
    id_convnet = convnet.id == id;
    id_cbds = cbds.id == id;
    id_borman = borman.id == id;
    id_luxand = luxand.id == id;
    id_winPhone = winPhone.id == id;
    if any(id_convnet) && any(id_cbds) && any(id_borman) &&...
            any(id_luxand) && any(id_winPhone)
        c1 = c1 + 1;
        gro = ground.pt(n1, :);
        dist = sqrt((gro(1) - gro(3)) .^ 2 + (gro(2) - gro(4)) .^ 2);
        poi = convnet.pt(id_convnet, :);
        err_convnet(c1, :) = sqrt(sum(reshape((gro - poi) .^ 2, 2, 5))) / dist;
        poi = cbds.pt(id_cbds, :);
        err_cbds(c1, :) = sqrt(sum(reshape((gro - poi) .^ 2, 2, 5))) / dist;
        poi = borman.pt(id_borman, :);
        err_borman(c1, :) = sqrt(sum(reshape((gro - poi) .^ 2, 2, 5))) / dist;
        poi = luxand.pt(id_luxand, :);
        err_luxand(c1, :) = sqrt(sum(reshape((gro - poi) .^ 2, 2, 5))) / dist;
        poi = winPhone.pt(id_winPhone, :);
        err_winPhone(c1, :) = sqrt(sum(reshape((gro(7 : 10) - poi) .^ 2, 2, 2))) / dist;
    end
end

err_convnet = err_convnet(1 : c1, :);
err_cbds = err_cbds(1 : c1, :);
err_borman = err_borman(1 : c1, :);
err_luxand = err_luxand(1 : c1, :);
err_winPhone = err_winPhone(1 : c1, :);

avgErr_convnet = mean(err_convnet) .* 100;
avgErr_cbds = mean(err_cbds) .* 100;
avgErr_borman = mean(err_borman) .* 100;
avgErr_luxand = mean(err_luxand) .* 100;
avgErr_winPhone = mean(err_winPhone) .* 100;

fail_convnet = sum(err_convnet > 0.1) ./ c1 .* 100;
fail_cbds = sum(err_cbds > 0.1) ./ c1 .* 100;
fail_borman = sum(err_borman > 0.1) ./ c1 .* 100;
fail_luxand = sum(err_luxand > 0.1) ./ c1 .* 100;
fail_winPhone = sum(err_winPhone > 0.1) ./ c1 .* 100;

fid1 = fopen('compare_bioid.txt', 'w');
fprintf(fid1, 'image compared: %d\r\n\r\n', c1);

fprintf(fid1, 'average normalized error (%%)\r\n\r\n');

fprintf(fid1, 'convnet\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    avgErr_convnet(1), avgErr_convnet(2), avgErr_convnet(3), avgErr_convnet(4), avgErr_convnet(5));
fprintf(fid1, 'cbds\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    avgErr_cbds(1), avgErr_cbds(2), avgErr_cbds(3), avgErr_cbds(4), avgErr_cbds(5));
fprintf(fid1, 'borman\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    avgErr_borman(1), avgErr_borman(2), avgErr_borman(3), avgErr_borman(4), avgErr_borman(5));
fprintf(fid1, 'luxand\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    avgErr_luxand(1), avgErr_luxand(2), avgErr_luxand(3), avgErr_luxand(4), avgErr_luxand(5));
fprintf(fid1, 'winPhone\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    avgErr_winPhone(1), avgErr_winPhone(2));

fprintf(fid1, 'failure rate (%%)\r\n\r\n');

fprintf(fid1, 'convnet\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    fail_convnet(1), fail_convnet(2), fail_convnet(3), fail_convnet(4), fail_convnet(5));
fprintf(fid1, 'cbds\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    fail_cbds(1), fail_cbds(2), fail_cbds(3), fail_cbds(4), fail_cbds(5));
fprintf(fid1, 'borman\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    fail_borman(1), fail_borman(2), fail_borman(3), fail_borman(4), fail_borman(5));
fprintf(fid1, 'luxand\r\nLE %.2f\r\nRE %.2f\r\nN  %.2f\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    fail_luxand(1), fail_luxand(2), fail_luxand(3), fail_luxand(4), fail_luxand(5));
fprintf(fid1, 'winPhone\r\nLM %.2f\r\nRM %.2f\r\n\r\n',...
    fail_winPhone(1), fail_winPhone(2));

fclose('all');
